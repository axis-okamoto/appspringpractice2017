
/* Drop Tables */

DROP TABLE ACCOUNT CASCADE CONSTRAINTS;
DROP TABLE BOOK CASCADE CONSTRAINTS;




/* Create Tables */

-- アカウント
CREATE TABLE ACCOUNT
(
	-- アカウントID
	ACCOUNT_ID varchar2(30) NOT NULL,
	-- 氏名
	NAME varchar2(100) NOT NULL,
	-- ステータス : ゴールド/シルバー/ブロンズ
	STATUS varchar2(12) NOT NULL,
	-- 削除フラグ : 0=false/1=true
	DELETE_FLG char(1) NOT NULL,
	PRIMARY KEY (ACCOUNT_ID)
);


-- 本
CREATE TABLE BOOK
(
	-- 本ID
	BOOK_ID varchar2(30) NOT NULL,
	-- タイトル
	TITLE varchar2(100) NOT NULL,
	-- カテゴリ : ファンタジー/ミステリー/歴史/ビジネス/哲学
	CATEGORY varchar2(30) NOT NULL,
	-- 価格
	PRICE number NOT NULL,
	-- 在庫状況 : あり/なし
	STOCK_STATUS char(6),
	-- 削除フラグ : 0=false/1=true
	DELETE_FLG char(1) NOT NULL,
	PRIMARY KEY (BOOK_ID)
);



/* Comments */

COMMENT ON TABLE ACCOUNT IS 'アカウント';
COMMENT ON COLUMN ACCOUNT.ACCOUNT_ID IS 'アカウントID';
COMMENT ON COLUMN ACCOUNT.NAME IS '氏名';
COMMENT ON COLUMN ACCOUNT.STATUS IS 'ステータス : ゴールド/シルバー/ブロンズ';
COMMENT ON COLUMN ACCOUNT.DELETE_FLG IS '削除フラグ : 0=false/1=true';
COMMENT ON TABLE BOOK IS '本';
COMMENT ON COLUMN BOOK.BOOK_ID IS '本ID';
COMMENT ON COLUMN BOOK.TITLE IS 'タイトル';
COMMENT ON COLUMN BOOK.CATEGORY IS 'カテゴリ : ファンタジー/ミステリー/歴史/ビジネス/哲学';
COMMENT ON COLUMN BOOK.PRICE IS '価格';
COMMENT ON COLUMN BOOK.STOCK_STATUS IS '在庫状況 : あり/なし';
COMMENT ON COLUMN BOOK.DELETE_FLG IS '削除フラグ : 0=false/1=true';



