package com.axis.springboot.answer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.axis.springboot.answer.form.AnsSearchBookForm;
import com.axis.springboot.answer.model.AnsBookModel;
import com.axis.springboot.answer.repository.AnsBookRepository;

@Service
public class AnsSearchBookService {

	@Autowired
	AnsBookRepository bookRepository;

	public List<AnsBookModel> searchAll() {
		return bookRepository.findAll();
	}

	public List<AnsBookModel> searchByTitle(AnsSearchBookForm form) {
		List<AnsBookModel> bookList = null;
		if(StringUtils.isEmpty(form.getTitle())){
			bookList = bookRepository.findAll();
		}else{
			bookList = bookRepository.findByTitle(form.getTitle());
		}
		return bookList;
	}

}
