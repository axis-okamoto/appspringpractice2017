package com.axis.springboot.answer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axis.springboot.answer.form.AnsRegisterAccountForm;
import com.axis.springboot.answer.model.AnsAccountModel;
import com.axis.springboot.answer.repository.AnsAccountRepository;

@Service
public class AnsRegisterAccountService {

	@Autowired
	AnsAccountRepository accontrepository;

	static final String NOT_DELETE = "0";

	@Transactional
	public boolean register(AnsRegisterAccountForm form) {

		AnsAccountModel accountModel = accontrepository.findByAccountId(form.getAccountId());

		if (accountModel != null) {
			return false;

		}
		accontrepository.insert(form.getAccountId(), form.getAccountName(), form.getAccountStatus(), NOT_DELETE);
		return true;
	}

}
