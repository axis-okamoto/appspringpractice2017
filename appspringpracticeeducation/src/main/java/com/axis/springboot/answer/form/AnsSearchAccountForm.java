package com.axis.springboot.answer.form;

public class AnsSearchAccountForm {

	private String accountStatus;

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

}
