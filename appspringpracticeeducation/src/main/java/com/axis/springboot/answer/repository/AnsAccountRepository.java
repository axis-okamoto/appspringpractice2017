package com.axis.springboot.answer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.axis.springboot.answer.model.AnsAccountModel;

@Repository
public interface AnsAccountRepository extends JpaRepository<AnsAccountModel, String> {

	@Query(value = "SELECT * FROM ACCOUNT WHERE ACCOUNT_ID = :accountId", nativeQuery = true)
	public AnsAccountModel findByAccountId(@Param("accountId") String accountId);

	@Query(value = "INSERT INTO ACCOUNT VALUES (:accountId,:name,:status,:deleteFlg)", nativeQuery = true)
	@Modifying
	public void insert(@Param("accountId") String accountId, @Param("name") String name, @Param("status") String status,
			@Param("deleteFlg") String deleteFlg);

	@Query(value = "SELECT * FROM ACCOUNT ORDER BY ACCOUNT_ID", nativeQuery = true)
	public List<AnsAccountModel> findAll();

	@Query(value = "SELECT * FROM ACCOUNT WHERE STATUS = :accountStatus ORDER BY ACCOUNT_ID", nativeQuery = true)
	public List<AnsAccountModel> findByStatus(@Param("accountStatus") String accountStatus);

	@Query(value = "UPDATE ACCOUNT SET NAME = :name ,STATUS = :status WHERE ACCOUNT_ID = :accountId", nativeQuery = true)
	@Modifying
	public void update(@Param("name") String name, @Param("status") String status,
			@Param("accountId") String accountId);

	@Query(value = "UPDATE ACCOUNT SET DELETE_FLG = :deleteFlg WHERE ACCOUNT_ID = :accountId", nativeQuery = true)
	@Modifying
	public void delete(@Param("deleteFlg") String deleteFlg, @Param("accountId") String accountId);

}
