package com.axis.springboot.answer.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.axis.springboot.answer.dto.AnsStatusDto;
import com.axis.springboot.answer.form.AnsRegisterAccountForm;
import com.axis.springboot.answer.service.AnsRegisterAccountService;

@Controller
public class AnsRegisterAccountController {

	@Autowired
	AnsRegisterAccountService registerAccountService;

	@RequestMapping(value = "/account/register/answer", method = RequestMethod.GET)
	public ModelAndView index(AnsRegisterAccountForm form, ModelAndView mv) {
		mv.addObject("statusList", createStatusList());
		mv.setViewName("/answer/account/register");
		return mv;
	}

	@RequestMapping(value = "/account/register/answer", method = RequestMethod.POST)
	public ModelAndView register(@Valid @ModelAttribute AnsRegisterAccountForm form, BindingResult result,
			ModelAndView mv) {
		if (result.hasErrors()) {
			mv.addObject("statusList", createStatusList());
			mv.setViewName("/answer/account/register");
			return mv;
		}
		if(!registerAccountService.register(form)){
			result.reject("message.err.register.account");
			mv.addObject("statusList", createStatusList());
			mv.setViewName("/answer/account/register");
			return mv;
		}
		return new ModelAndView("redirect:/account/register/answer");
	}

	private List<AnsStatusDto> createStatusList() {
		List<AnsStatusDto> statusList = new ArrayList<>();
		AnsStatusDto gold = new AnsStatusDto("ゴールド", "ゴールド");
		AnsStatusDto silver = new AnsStatusDto("シルバー", "シルバー");
		AnsStatusDto bronze = new AnsStatusDto("ブロンズ", "ブロンズ");
		statusList.add(gold);
		statusList.add(silver);
		statusList.add(bronze);
		return statusList;
	}

}
