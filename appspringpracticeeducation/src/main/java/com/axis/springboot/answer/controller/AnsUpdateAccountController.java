package com.axis.springboot.answer.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.axis.springboot.answer.dto.AnsStatusDto;
import com.axis.springboot.answer.form.AnsUpdateAccountForm;
import com.axis.springboot.answer.model.AnsAccountModel;
import com.axis.springboot.answer.service.AnsUpdateAccountService;

@Controller
public class AnsUpdateAccountController {

	@Autowired
	AnsUpdateAccountService ansUpdateAccountService;

	@RequestMapping(value = "/account/update/answer", method = RequestMethod.GET)
	public ModelAndView index(@RequestParam String accountId, AnsUpdateAccountForm form, ModelAndView mv) {
		AnsAccountModel accountModel = ansUpdateAccountService.findByAccountId(accountId);
		form.setName(accountModel.getName());
		form.setAccountStatus(accountModel.getAccountStatus());
		form.setAccountId(accountModel.getAccountId());
		mv.addObject("statusList", createStatusList());
		mv.setViewName("answer/account/update");
		return mv;
	}

	@RequestMapping(value = "/account/update/answer", method = RequestMethod.POST, params = "update")
	public ModelAndView update(@Valid @ModelAttribute AnsUpdateAccountForm form, BindingResult result,
			ModelAndView mv) {
		if (result.hasErrors()) {
			mv.addObject("statusList", createStatusList());
			mv.setViewName("answer/account/update");
			return mv;
		}
		boolean successFlg = ansUpdateAccountService.update(form);
		if (!successFlg) {
			mv.setViewName("answer/error");
			return mv;
		}
		return new ModelAndView("redirect:/account/update/answer?accountId=" + form.getAccountId());
	}

	@RequestMapping(value = "/account/update/answer", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@ModelAttribute AnsUpdateAccountForm form, ModelAndView mv) {
		boolean successFlg = ansUpdateAccountService.delete(form.getAccountId());
		if (!successFlg) {
			mv.setViewName("answer/error");
			return mv;
		}
		return new ModelAndView("redirect:/account/search/answer");
	}

	private List<AnsStatusDto> createStatusList() {
		List<AnsStatusDto> statusList = new ArrayList<>();
		AnsStatusDto gold = new AnsStatusDto("ゴールド", "ゴールド");
		AnsStatusDto silver = new AnsStatusDto("シルバー", "シルバー");
		AnsStatusDto bronze = new AnsStatusDto("ブロンズ", "ブロンズ");
		statusList.add(gold);
		statusList.add(silver);
		statusList.add(bronze);
		return statusList;
	}

}
