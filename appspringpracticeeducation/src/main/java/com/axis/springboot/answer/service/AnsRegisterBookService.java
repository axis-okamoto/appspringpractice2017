package com.axis.springboot.answer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axis.springboot.answer.form.AnsRegisterBookForm;
import com.axis.springboot.answer.model.AnsBookModel;
import com.axis.springboot.answer.repository.AnsBookRepository;

@Service
public class AnsRegisterBookService {

	@Autowired
	AnsBookRepository bookRepository;

	static final String NOT_DELETE = "0";

	@Transactional
	public boolean register(AnsRegisterBookForm form) {

		AnsBookModel bookModel = bookRepository.findByBookId(form.getBookId());
		if (bookModel != null) {
			return false;
		}

		bookRepository.insert(form.getBookId(), form.getTitle(), form.getCategory(), form.getPrice(),
				form.getStockStatus(), NOT_DELETE);
		return true;
	}

}
