package com.axis.springboot.answer.form;

public class AnsSearchBookForm {

	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
