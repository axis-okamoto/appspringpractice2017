package com.axis.springboot.answer.dto;

public class AnsCategoryDto {

	private String categoryKey;

	private String categoryValue;

	public AnsCategoryDto(String key, String value) {
		this.categoryKey = key;
		this.categoryValue = value;
	}

	public String getCategoryKey() {
		return categoryKey;
	}

	public void setCategoryKey(String categoryKey) {
		this.categoryKey = categoryKey;
	}

	public String getCategoryValue() {
		return categoryValue;
	}

	public void setCategoryValue(String categoryValue) {
		this.categoryValue = categoryValue;
	}

}
