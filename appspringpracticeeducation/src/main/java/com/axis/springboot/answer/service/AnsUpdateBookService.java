package com.axis.springboot.answer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axis.springboot.answer.form.AnsUpdateBookForm;
import com.axis.springboot.answer.model.AnsBookModel;
import com.axis.springboot.answer.repository.AnsBookRepository;

@Service
public class AnsUpdateBookService {

	static final String ALREADY_DELETE = "1";

	@Autowired
	AnsBookRepository bookRepository;

	public AnsBookModel findByBookId(String bookId) {
		AnsBookModel bookModel = bookRepository.findByBookId(bookId);
		return bookModel;
	}

	@Transactional
	public boolean update(AnsUpdateBookForm form) {
		AnsBookModel bookModel = bookRepository.findByBookId(form.getBookId());
		if (bookModel == null) {
			return false;
		}
		bookRepository.update(form.getTitle(), form.getCategory(), form.getPrice(), form.getStockStatus(),
				form.getBookId());
		return true;
	}

	@Transactional
	public boolean delete(String bookId) {
		AnsBookModel bookModel = bookRepository.findByBookId(bookId);
		if (bookModel == null) {
			return false;
		}
		bookRepository.delete(ALREADY_DELETE, bookId);
		return true;
	}

}
