package com.axis.springboot.answer.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.axis.springboot.answer.dto.AnsCategoryDto;
import com.axis.springboot.answer.dto.AnsStockStatusDto;
import com.axis.springboot.answer.form.AnsUpdateBookForm;
import com.axis.springboot.answer.model.AnsBookModel;
import com.axis.springboot.answer.service.AnsUpdateBookService;

@Controller
public class AnsUpdateBookController {

    @Autowired
    AnsUpdateBookService updateBookService;

    @RequestMapping(value = "/book/update/answer", method = RequestMethod.GET)
    public ModelAndView index(@RequestParam String bookId, AnsUpdateBookForm form, ModelAndView mv) {
        AnsBookModel book = updateBookService.findByBookId(bookId);
        form.setTitle(book.getTitle());
        form.setCategory(book.getCategory());
        form.setPrice(book.getPrice());
        form.setStockStatus(book.getStockStatus());
        form.setBookId(book.getBookId());

        mv.addObject("categoryList", createCategoryDto());
        mv.addObject("stockStatusList", createStockStatusDto());
        mv.setViewName("answer/book/update");
        return mv;
    }

    @RequestMapping(value = "/book/update/answer", method = RequestMethod.POST, params = "update")
    public ModelAndView update(@Valid @ModelAttribute AnsUpdateBookForm form, BindingResult result, ModelAndView mv) {
        if (result.hasErrors()) {
            mv.addObject("categoryList", createCategoryDto());
            mv.addObject("stockStatusList", createStockStatusDto());
            mv.setViewName("answer/book/update");
            return mv;
        }
        boolean successFlg = updateBookService.update(form);
        if (!successFlg) {
            mv.setViewName("answer/error");
            return mv;
        }
        return new ModelAndView("redirect:/book/update/answer?bookId=" + form.getBookId());
    }

    @RequestMapping(value = "/book/update/answer", method = RequestMethod.POST, params = "delete")
    public ModelAndView delete(@ModelAttribute AnsUpdateBookForm form, ModelAndView mv) {
        boolean successFlg = updateBookService.delete(form.getBookId());
        if (!successFlg) {
            mv.setViewName("answer/error");
            return mv;
        }
        return new ModelAndView("redirect:/book/search/answer");
    }

    private List<AnsCategoryDto> createCategoryDto() {
        List<AnsCategoryDto> categoryList = new ArrayList<>();
        categoryList.add(new AnsCategoryDto("ファンタジー", "ファンタジー"));
        categoryList.add(new AnsCategoryDto("ミステリー", "ミステリー"));
        categoryList.add(new AnsCategoryDto("歴史", "歴史"));
        categoryList.add(new AnsCategoryDto("ビジネス", "ビジネス"));
        categoryList.add(new AnsCategoryDto("哲学", "哲学"));
        return categoryList;
    }

    private List<AnsStockStatusDto> createStockStatusDto() {
        List<AnsStockStatusDto> stockStatusList = new ArrayList<>();
        stockStatusList.add(new AnsStockStatusDto("あり", "あり"));
        stockStatusList.add(new AnsStockStatusDto("なし", "なし"));
        return stockStatusList;
    }

}
