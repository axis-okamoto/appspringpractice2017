package com.axis.springboot.answer.dto;

public class AnsStockStatusDto {

	private String stockStatusKey;

	private String stockStatusValue;

	public AnsStockStatusDto(String key, String value) {
		this.stockStatusKey = key;
		this.stockStatusValue = value;
	}

	public String getStockStatusKey() {
		return stockStatusKey;
	}

	public void setStockStatusKey(String stockStatusKey) {
		this.stockStatusKey = stockStatusKey;
	}

	public String getStockStatusValue() {
		return stockStatusValue;
	}

	public void setStockStatusValue(String stockStatusValue) {
		this.stockStatusValue = stockStatusValue;
	}

}
