package com.axis.springboot.answer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.axis.springboot.answer.form.AnsUpdateAccountForm;
import com.axis.springboot.answer.model.AnsAccountModel;
import com.axis.springboot.answer.repository.AnsAccountRepository;

@Service
public class AnsUpdateAccountService {

	static final String ALREADY_DELETED = "1";

	@Autowired
	AnsAccountRepository accountRepository;

	public AnsAccountModel findByAccountId(String accountId) {
		AnsAccountModel accountModel = accountRepository.findByAccountId(accountId);
		return accountModel;
	}

	@Transactional
	public boolean update(AnsUpdateAccountForm form) {
		AnsAccountModel accountModel = accountRepository.findByAccountId(form.getAccountId());
		if (accountModel == null) {
			return false;
		}
		accountRepository.update(form.getName(), form.getAccountStatus(), form.getAccountId());
		return true;
	}

	@Transactional
	public boolean delete(String accountId) {
		AnsAccountModel accountModel = accountRepository.findByAccountId(accountId);
		if (accountModel == null) {
			return false;
		}
		accountRepository.delete(ALREADY_DELETED, accountId);
		return true;

	}
}
