package com.axis.springboot.answer.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.axis.springboot.answer.dto.AnsStatusDto;
import com.axis.springboot.answer.form.AnsSearchAccountForm;
import com.axis.springboot.answer.model.AnsAccountModel;
import com.axis.springboot.answer.service.AnsSearchAccountService;

@Controller
public class AnsSearchAccountController {

	@Autowired
	AnsSearchAccountService searchAccountService;

	@RequestMapping(value = "/account/search/answer", method = RequestMethod.GET)
	public ModelAndView index(ModelAndView mv, AnsSearchAccountForm form) {
		List<AnsAccountModel> accountList = searchAccountService.searchAll();
		mv.addObject("accountList", accountList);
		mv.addObject("statusList", createStatusList());
		mv.setViewName("answer/account/search");
		return mv;
	}

	@RequestMapping(value = "/account/search/answer", method = RequestMethod.POST)
	public ModelAndView search(@ModelAttribute AnsSearchAccountForm form, ModelAndView mv) {
		List<AnsAccountModel> accountList = searchAccountService.searchByStatus(form);
		mv.addObject("accountList", accountList);
		mv.addObject("statusList", createStatusList());
		mv.setViewName("answer/account/search");
		return mv;
	}

	private List<AnsStatusDto> createStatusList() {
		List<AnsStatusDto> statusList = new ArrayList<>();
		AnsStatusDto gold = new AnsStatusDto("ゴールド", "ゴールド");
		AnsStatusDto silver = new AnsStatusDto("シルバー", "シルバー");
		AnsStatusDto bronze = new AnsStatusDto("ブロンズ", "ブロンズ");
		statusList.add(gold);
		statusList.add(silver);
		statusList.add(bronze);
		return statusList;
	}

}
