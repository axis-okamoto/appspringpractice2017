package com.axis.springboot.answer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.axis.springboot.answer.model.AnsBookModel;

@Repository
public interface AnsBookRepository extends JpaRepository<AnsBookModel, String> {

	@Query(value = "INSERT INTO BOOK VALUES (:bookId,:title,:category,:price,:stockStatus,:deleteFlg)", nativeQuery = true)
	@Modifying
	public void insert(@Param("bookId") String bookId, @Param("title") String title, @Param("category") String category,
			@Param("price") Integer price, @Param("stockStatus") String stockStatus,
			@Param("deleteFlg") String deleteFlg);

	@Query(value = "SELECT * FROM BOOK WHERE BOOK_ID = :bookId", nativeQuery = true)
	public AnsBookModel findByBookId(@Param("bookId") String bookId);

	@Query(value = "SELECT * FROM BOOK ORDER BY BOOK_ID", nativeQuery = true)
	public List<AnsBookModel> findAll();

	@Query(value = "SELECT * FROM BOOK WHERE TITLE LIKE %:title% ORDER BY BOOK_ID", nativeQuery = true)
	public List<AnsBookModel> findByTitle(@Param("title") String title);

	@Query(value = "UPDATE BOOK SET TITLE = :title, CATEGORY = :category, PRICE = :price, STOCK_STATUS= :stockStatus WHERE BOOK_ID = :bookId", nativeQuery = true)
	@Modifying
	public void update(@Param("title") String title, @Param("category") String category, @Param("price") Integer price,
			@Param("stockStatus") String stockStatus, @Param("bookId") String bookId);

	@Query(value = "UPDATE BOOK SET DELETE_FLG = :deleteFlg WHERE BOOK_ID = :bookId", nativeQuery = true)
	@Modifying
	public void delete(@Param("deleteFlg") String deleteFlg, @Param("bookId") String bookId);

}
