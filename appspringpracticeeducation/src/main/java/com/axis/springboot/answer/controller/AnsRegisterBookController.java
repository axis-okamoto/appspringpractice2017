package com.axis.springboot.answer.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.axis.springboot.answer.dto.AnsCategoryDto;
import com.axis.springboot.answer.dto.AnsStockStatusDto;
import com.axis.springboot.answer.form.AnsRegisterBookForm;
import com.axis.springboot.answer.service.AnsRegisterBookService;

@Controller
public class AnsRegisterBookController {

	@Autowired
	AnsRegisterBookService registerBookService;

	@RequestMapping(value = "/book/register/answer", method = RequestMethod.GET)
	public ModelAndView index(AnsRegisterBookForm form, ModelAndView mv) {

		mv.addObject("categoryList", createCategoryDto());
		mv.addObject("stockStatusList", createStockStatusDto());
		mv.setViewName("/answer/book/register");
		return mv;
	}

	@RequestMapping(value = "/book/register/answer", method = RequestMethod.POST)
	public ModelAndView register(@Valid @ModelAttribute AnsRegisterBookForm form, BindingResult result, ModelAndView mv) {
		if (result.hasErrors()) {
			mv.addObject("categoryList", createCategoryDto());
			mv.addObject("stockStatusList", createStockStatusDto());
			mv.setViewName("/answer/book/register");
			return mv;
		}

		if(!registerBookService.register(form)){
			result.reject("message.err.register.book");
			mv.addObject("categoryList", createCategoryDto());
			mv.addObject("stockStatusList", createStockStatusDto());
			mv.setViewName("/answer/book/register");
			return mv;
		}
		return new ModelAndView("redirect:/book/register/answer");
	}

	private List<AnsCategoryDto> createCategoryDto() {
		List<AnsCategoryDto> categoryList = new ArrayList<>();
		categoryList.add(new AnsCategoryDto("ファンタジー", "ファンタジー"));
		categoryList.add(new AnsCategoryDto("ミステリー", "ミステリー"));
		categoryList.add(new AnsCategoryDto("歴史", "歴史"));
		categoryList.add(new AnsCategoryDto("ビジネス", "ビジネス"));
		categoryList.add(new AnsCategoryDto("哲学", "哲学"));
		return categoryList;
	}

	private List<AnsStockStatusDto> createStockStatusDto() {
		List<AnsStockStatusDto> stockStatusList = new ArrayList<>();
		stockStatusList.add(new AnsStockStatusDto("あり", "あり"));
		stockStatusList.add(new AnsStockStatusDto("なし", "なし"));
		return stockStatusList;
	}

}
