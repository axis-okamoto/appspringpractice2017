package com.axis.springboot.answer.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.axis.springboot.answer.form.AnsSearchBookForm;
import com.axis.springboot.answer.model.AnsBookModel;
import com.axis.springboot.answer.service.AnsSearchBookService;

@Controller
public class AnsSearchBookController {

	@Autowired
	AnsSearchBookService searchBookService;

	@RequestMapping(value="/book/search/answer" , method = RequestMethod.GET)
	public ModelAndView index(AnsSearchBookForm form , ModelAndView mv){
		List<AnsBookModel> bookList = searchBookService.searchAll();
		mv.addObject("bookList", bookList);
		mv.setViewName("answer/book/search");
		return mv;
	}

	@RequestMapping(value="/book/search/answer" , method = RequestMethod.POST)
	public ModelAndView search(@ModelAttribute AnsSearchBookForm form , ModelAndView mv){
		List<AnsBookModel> bookList = searchBookService.searchByTitle(form);
		mv.addObject("bookList", bookList);
		mv.setViewName("answer/book/search");
		return mv;
	}

}
