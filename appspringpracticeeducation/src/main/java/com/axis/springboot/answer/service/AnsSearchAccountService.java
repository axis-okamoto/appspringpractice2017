package com.axis.springboot.answer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.axis.springboot.answer.form.AnsSearchAccountForm;
import com.axis.springboot.answer.model.AnsAccountModel;
import com.axis.springboot.answer.repository.AnsAccountRepository;

@Service
public class AnsSearchAccountService {

	@Autowired
	AnsAccountRepository accountRepository;

	public List<AnsAccountModel> searchAll(){
		return accountRepository.findAll();
	}

	public List<AnsAccountModel> searchByStatus(AnsSearchAccountForm form){
		List<AnsAccountModel> accountList = null;
		if (StringUtils.isEmpty(form.getAccountStatus())) {
			accountList = accountRepository.findAll();
		} else {
			accountList = accountRepository.findByStatus(form.getAccountStatus());
		}
		return accountList;
	}

}
