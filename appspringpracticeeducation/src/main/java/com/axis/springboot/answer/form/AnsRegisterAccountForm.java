package com.axis.springboot.answer.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class AnsRegisterAccountForm {
	@NotEmpty
	@Length(min = 1, max = 10)
	private String accountId;

	@NotEmpty
	@Length(min = 1, max = 30)
	private String accountName;

	@NotEmpty
	private String accountStatus;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

}
