<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>アカウント更新</title>
</head>
<body>
	<div>
		<h3>アカウント更新画面</h3>
	</div>
	<div>
		<form:form modelAttribute="ansUpdateAccountForm"
			action="/account/update/answer" method="post">
			<form:errors path="*" element="div" />
			<div>名前</div>
			<div>
				<form:input path="name" />
			</div>
			<br>
			<div>ステータス</div>
			<div>
				<form:select path="accountStatus">
					<form:options items="${statusList}" itemLabel="value"
						itemValue="key" />
				</form:select>
			</div>
			<br>
			<div>
				<input type="submit" value="更新" name=update />
				<input type="submit" value="削除" name=delete />
				<form:hidden path="accountId" />
			</div>
		</form:form>
	</div>
</body>
</html>