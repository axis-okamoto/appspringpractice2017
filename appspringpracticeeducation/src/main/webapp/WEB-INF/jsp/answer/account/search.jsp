<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>アカウント検索</title>
</head>
<body>
	<div>
		<h3>アカウント検索画面</h3>
	</div>
	<div>
		<form:form modelAttribute="ansSearchAccountForm"
			action="/account/search/answer" method="post">
			<div>
				ステータス
				<form:select path="accountStatus">
					<form:option value="" label="-" />
					<form:options items="${statusList}" itemLabel="value"
						itemValue="key" />
				</form:select>
			</div>
			<div>
				<input type="submit" value="検索" />
			</div>
		</form:form>
	</div>
	<br>
	<div>
		<table border="1">
			<thead>
				<tr>
					<th>No</th>
					<th>アカウントID</th>
					<th>名前</th>
					<th>ステータス</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${accountList}" var="account" varStatus="staus">
					<tr>
						<th><c:out value="${staus.index +1}" /></th>
						<th><c:out value="${account.accountId}" /></th>
						<th><c:out value="${account.name}" /></th>
						<th><c:out value="${account.accountStatus}" /></th>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>


</body>
</html>