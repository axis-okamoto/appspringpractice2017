<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>書籍検索</title>
</head>
<body>
	<div>
		<h3>書籍検索画面</h3>
	</div>
	<div>
		<form:form modelAttribute="ansSearchBookForm"
			action="/book/search/answer" method="post">
			<div>
				タイトル  <form:input path="title" />
			</div>
			<div>
				<input type="submit" value="検索" />
			</div>
		</form:form>
	</div>
	<br>
	<div>
		<table border="1">
			<thead>
				<tr>
					<th>No</th>
					<th>書籍ID</th>
					<th>タイトル</th>
					<th>カテゴリ</th>
					<th>価格</th>
					<th>在庫状況</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${bookList}" var="book" varStatus="status">
					<tr>
						<th><c:out value="${status.index+1}"></c:out></th>
						<th><c:out value="${book.bookId}"></c:out></th>
						<th><c:out value="${book.title}"></c:out></th>
						<th><c:out value="${book.category}"></c:out></th>
						<th><fmt:formatNumber value="${book.price}" pattern="###,###" /></th>
						<th><c:out value="${book.stockStatus}"></c:out></th>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>