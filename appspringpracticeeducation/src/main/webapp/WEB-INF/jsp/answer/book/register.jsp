<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>書籍登録</title>
</head>
<body>
	<div>
		<h3>書籍登録画面</h3>
	</div>
	<div>
		<form:form modelAttribute="ansRegisterBookForm"
			action="/book/register/answer" method="post">
			<form:errors path="*" element="div" />
			<div>書籍ID</div>
			<div>
				<form:input path="bookId" />
			</div>
			<br>
			<div>タイトル</div>
			<div>
				<form:input path="title" />
			</div>
			<br>
			<div>カテゴリ</div>
			<div>
				<form:select path="category">
					<form:options items="${categoryList}" itemLabel="categoryValue"
						itemValue="categoryKey" />
				</form:select>
			</div>
			<br>
			<div>価格</div>
			<div>
				<form:input path="price" />
			</div>
			<br>
			<div>在庫状況</div>
			<div>
				<form:radiobuttons path="stockStatus" items="${stockStatusList}"
					itemLabel="stockStatusValue" itemValue="stockStatusKey" />
			</div>
			<br>
			<div>
				<input type="submit" value="登録" />
			</div>
		</form:form>
	</div>
</body>
</html>